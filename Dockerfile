FROM openjdk:8-jdk-alpine

MAINTAINER HungDoan

COPY target/Task4-0.0.1-SNAPSHOT.jar /home/app.jar

WORKDIR /home

ENTRYPOINT ["java","-jar","app.jar"]
