drop database if exists freetuts;

use freetuts;
INSERT INTO ROLES (name, active)
VALUES 
 ("ADMIN", true),
 ("MOD", true);
 
INSERT INTO USERS (name, email, password, username, role_id, active)
VALUES 
 ("Vuong", "Vuong@gmail.com", "$2b$10$KH.7cpN5YavE.UM1x9Mhvu99a0moldgDQ8XV2vE4Ab9hfRe7FKpBq", "VuongNT13", 1, true),
 ("Hung", "Hung@gmail.com", "$2b$10$KH.7cpN5YavE.UM1x9Mhvu99a0moldgDQ8XV2vE4Ab9hfRe7FKpBq", "HungDQ24", 2, true);
 
INSERT INTO Categories (name, category_id, active)
VALUES 
 ("Programming", null ,true),
 ("Java", 1, true),
 ("PHP", 1, true),
 ("Coupon", null, true),
 ("Tricky Tips", null, true),
 ("Database", 1, true),
 ("Course", null ,true),
 ("Java Courses", 7 ,true),
 ("PHP Courses", 7 ,true),
 ("Database Courses", 7 ,true);
 
INSERT INTO Posts (title, content, description, cover_image, category_id, created_by, created_at, active)
VALUES 
 ("Java In Action", "This is content", "This is a good content for a java course", "https://dizibrand.com/wp-content/uploads/2021/02/Java-Dizibrand-1.jpg", 2, 1, CURRENT_TIME(), true),
 ("Multithreading in java", "This is content", "This is a good content for a java course", "https://dizibrand.com/wp-content/uploads/2021/02/Java-Dizibrand-1.jpg", 2, 1, CURRENT_TIME(), true),
 ("Java In Action 2", "This is content", "This is a good content for a java course", "https://bitly.com.vn/lvlqo8", 2, 1, CURRENT_TIME(), true),
 ("Multithreading in java 2", "This is content", "This is a good content for a java course", "https://bitly.com.vn/lvlqo8", 2, 1, CURRENT_TIME(), true),
 ("PHP Web Programming", "This is content", "This is a good content for a java course", "https://bitly.com.vn/p4638v", 3, 1, CURRENT_TIME(), true),
 ("PHP Web Programming 2", "This is content", "This is a good content for a java course","https://bitly.com.vn/5hg6se", 3, 1, CURRENT_TIME(), true),
 ("AAA Coupon", "This is content", "This is a exemtrely good allowance when you use this coupon", "https://freetuts.net/upload/blog_post/images/2021/04/08/71/giam-gia-kdata.JPG", 4, 1, CURRENT_TIME(), true),
 ("BBB Coupon", "This is content", "This is a exemtrely good allowance when you use this coupon", "https://freetuts.net/upload/blog_post/images/2021/01/25/69/tinohost-tet-2021.JPG", 4, 1, CURRENT_TIME(), true),
 ("CCC Coupon", "This is content", "This is a exemtrely good allowance when you use this coupon", "https://freetuts.net/upload/blog_post/images/2020/11/26/68/tinohost-sale-huy-diet.JPG", 4, 1, CURRENT_TIME(), true),
 ("DDD Coupon", "This is content", "This is a exemtrely good allowance when you use this coupon", "https://freetuts.net/upload/tut_post/images/2020/11/19/3363/bf-2020-1920x1080-3-1024x576.png", 4, 1, CURRENT_TIME(), true),
 ("How to intsall the lasted ubuntu version", "This is content", "This is a exemtrely good guide for you to use...", "https://www.chanchao.com.tw/vietnamwood/images/default.jpg", 5, 1, CURRENT_TIME(), true),
("How to remove totally Microsoft SQL Server in Window", "This is content", "This is a exemtrely good guide for you to use...", "https://www.chanchao.com.tw/vietnamwood/images/default.jpg", 5, 1, CURRENT_TIME(), true),
 ("Spring from beginner to guru", "This is content of this course", "This is a exemtrely good guide for you to use...", "https://spring.io/images/OG-Spring.png", 8, 1, CURRENT_TIME(), true),
 ("Introduce to spring cloud and microservice", "This is content of this course", "This is a exemtrely good guide for you to use...", "https://www.chanchao.com.tw/vietnamwood/images/default.jpg", 8, 1, CURRENT_TIME(), true),
 ("Build your own likely  Lavarel framework using PHP", "This is content of this course", "This is a exemtrely good guide for you to use...", "https://bitly.com.vn/5hg6se", 9, 1, CURRENT_TIME(), true),
 ("Deep dive into MySQL", "This is content of this course", "This is a exemtrely good guide for you to use...", "https://www.chanchao.com.tw/vietnamwood/images/default.jpg", 10, 1, CURRENT_TIME(), true);
 
INSERT INTO Courses (name, price, post_id, discount, active)
VALUES
("Spring from beginner to guru", 1000, 13, 15, true),
("Introduce to spring cloud and microservice", 2000, 14, 15, true),
("Build your own likely  Lavarel framework using PHP", 1500, 15, 15, true),
("Deep dive into MySQL", 3000, 16, 15, true);

INSERT INTO Invoices (customer_name, email, grand_total, discount, course_id, active)
VALUES
("Doan Quoc Hung", "hungdoan@gmail.com", 10000, 15, 1, true),
("Nguyen Phi Long", "long@gmail.com", 10400, 10, 1, true),
("Nguyen Hoan Hao", "hao@gmail.com", 25000, 15, 2, true);

INSERT INTO Invoices (customer_name, email, grand_total, discount, course_id, active)
VALUES
("Nguyen Thanh Vuong", "hungdoan@gmail.com", 10000, 15, 1, true),
("Nguyen Phi Hung", "long@gmail.com", 10400, 10, 1, true),
("Nguyen Halo", "hao@gmail.com", 25000, 15, 2, true);

INSERT INTO Posts (title, content, description, cover_image, category_id, created_by, created_at, active)
VALUES
 ("Java In Action 3", "This is content", "This is a good content for a java course", "https://dizibrand.com/wp-content/uploads/2021/02/Java-Dizibrand-1.jpg", 2, 1, CURRENT_TIME(), true),
 ("Multithreading in java 3", "This is content", "This is a good content for a java course", "https://bitly.com.vn/lvlqo8", 2, 1, CURRENT_TIME(), true),
 ("Multithreading in java 4", "This is content", "This is a good content for a java course", "https://bitly.com.vn/lvlqo8", 2, 1, CURRENT_TIME(), true);