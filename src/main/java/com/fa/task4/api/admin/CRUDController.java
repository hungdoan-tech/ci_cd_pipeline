package com.fa.task4.api.admin;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.fa.task4.dto.AbstractDTO;
import com.fa.task4.dto.Ids;
import com.fa.task4.mapper.IMapper;
import com.fa.task4.service.impl.CRUDService;

public abstract class CRUDController<T, DTO extends AbstractDTO<T>> {
	
	@Autowired
	private CRUDService<T> service;

	@Autowired
	private IMapper<T> mapper;

	@GetMapping
	public ResponseEntity<Object> getAll() {
		try {
			List<AbstractDTO<T>> dtos = mapper.convertEntitiesToDTOs(service.getAll());			
			return ResponseEntity.ok(dtos);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body("Exception: " + e.getClass());
		}
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<Object> get(@PathVariable(name = "id") Integer id) {
		try {
			T entity = service.getOne(id);
			AbstractDTO<T> dto = mapper.convertEntityToDTO(entity);
			return ResponseEntity.ok(dto);
		} catch (Exception e) {
			e.printStackTrace();			
			return ResponseEntity.badRequest().body("Exception: " + e.getClass());
		}
	}

	@SuppressWarnings("unchecked")
	@PostMapping
	public ResponseEntity<Object> create(@Valid @RequestBody DTO dto) {
		try {
			dto.setId(null);
			T entity = mapper.convertDTOToEntity(dto);
			entity = service.create(entity);
			dto = (DTO) mapper.convertEntityToDTO(entity);
			return ResponseEntity.ok().body(dto);
		} catch (Exception e) {
			e.printStackTrace();			
			return ResponseEntity.badRequest().body("Exception: " + e.getClass());
		}
	}

	@SuppressWarnings("unchecked")
	@PutMapping("/{id}")
	public ResponseEntity<Object> update(@RequestBody DTO dto,
			@PathVariable(name = "id") Integer id) {
		try {
			dto.setId(id);
			System.out.println(dto);
			T entity = mapper.convertDTOToEntity(dto);
			System.out.println(entity);
			entity = service.update(entity);
			System.out.println(entity);
			dto = (DTO) mapper.convertEntityToDTO(entity);			
			return ResponseEntity.ok(dto);
		} catch (Exception e) {
			e.printStackTrace();			
			return ResponseEntity.badRequest().body("Exception: " + e.getClass());
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> delete(@PathVariable(name = "id") Integer id) {
		try {
			Boolean result = service.deleteById(id);
			if (result) {				
				return ResponseEntity.ok("Delete this one successfully");
			} else {				
				return ResponseEntity.badRequest().body("Delete this one failed");
			}
		} catch (Exception e) {
			e.printStackTrace();			
			return ResponseEntity.badRequest().body("Exception: " + e.getClass());
		}
	}

	@DeleteMapping
	public ResponseEntity<Object> delete(@RequestBody Ids ids) {
		try {
			Boolean result = service.deleteByIds(ids.getIds());	
			if (result) {				
				return ResponseEntity.ok("Delete all successfully");
			} else {				
				return ResponseEntity.ok("Delete all failed");
			}
		} catch (Exception e) {
			e.printStackTrace();			
			return ResponseEntity.badRequest().body("Exception: " + e.getClass());
		}
	}
	
	@GetMapping("/paging")
	public ResponseEntity<Object> getPage(
			@RequestParam(name = "page", defaultValue = "0") Integer page,
			@RequestParam(name = "size", defaultValue = "10") Integer size, 
			@RequestParam(name = "sort_by", defaultValue = "id") String sortBy, 
			@RequestParam(name = "sort_type", defaultValue = "asc") String sortType) {
		return service.paging(page, size, sortBy, sortType);
	}
}
