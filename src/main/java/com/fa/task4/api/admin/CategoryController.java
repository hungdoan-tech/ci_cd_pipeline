package com.fa.task4.api.admin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fa.task4.dto.CategoryDTO;
import com.fa.task4.entity.Category;

@RestController
@RequestMapping("/api/categories")
public class CategoryController extends CRUDController<Category, CategoryDTO>{
	
}
