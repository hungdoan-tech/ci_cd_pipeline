package com.fa.task4.api.admin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fa.task4.dto.CommentDTO;
import com.fa.task4.entity.Comment;

@RestController
@RequestMapping("/api/comments")
public class CommentController extends CRUDController<Comment, CommentDTO>{
	
}
