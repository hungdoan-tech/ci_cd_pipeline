package com.fa.task4.api.admin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fa.task4.dto.CourseDTO;
import com.fa.task4.entity.Course;

@RestController
@RequestMapping("/api/courses")
public class CourseController extends CRUDController<Course, CourseDTO>{
	
}
