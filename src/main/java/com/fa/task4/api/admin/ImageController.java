package com.fa.task4.api.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fa.task4.service.impl.IAWSS3Service;

@Controller
@RequestMapping("/api/images")
public class ImageController {
	
	@Autowired
	private IAWSS3Service awss3Service;
	
	@PostMapping
	@CrossOrigin
	public ResponseEntity<String> uploadImage(@RequestParam("file") MultipartFile file) {
		try {						
			if(!file.isEmpty()) {
				String url = this.awss3Service.uploadFile(file);				
				return (url != null) ? ResponseEntity.ok().body(url) : ResponseEntity.badRequest().body("File is not valid");		
			}
			return ResponseEntity.badRequest().body("File is empty");
		} catch (Exception e) {
			e.printStackTrace();			
			return ResponseEntity.badRequest().body("Exception: " + e.getClass());
		}		
	}	
}
