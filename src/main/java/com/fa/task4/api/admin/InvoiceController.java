package com.fa.task4.api.admin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fa.task4.dto.InvoiceDTO;
import com.fa.task4.entity.Invoice;

@RestController
@RequestMapping("/api/invoices")
public class InvoiceController extends CRUDController<Invoice, InvoiceDTO> {	

}
