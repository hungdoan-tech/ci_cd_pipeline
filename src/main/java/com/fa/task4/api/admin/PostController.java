package com.fa.task4.api.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fa.task4.dto.PostDTO;
import com.fa.task4.entity.Post;
import com.fa.task4.entity.User;
import com.fa.task4.service.IPostService;
import com.fa.task4.service.IUserService;

@RestController
@RequestMapping("/api/posts")
@CrossOrigin
public class PostController extends CRUDController<Post, PostDTO>{
		
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IPostService postService;
	
	@Value("${task4.default.image}")
	private String defaultImage;
	
	@GetMapping("/search")
	public ResponseEntity<Object> getPostHaveTitleLikeKey(
			@RequestParam(name = "keyword") String keyword, 
			@RequestParam(name = "page", defaultValue = "0") Integer page) {
		return this.postService.listAllPostLikeSearchKey(keyword, page, 5, "id", "desc");		
	}	

	@Override
	public ResponseEntity<Object> create(@Valid @RequestBody PostDTO dto) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User authenticatedUser = this.userService.findOneByUsername(authentication.getName());
		dto.setAuthorId(authenticatedUser.getId());
		
		if(dto.getCoverImage() == null) {
			dto.setCoverImage(defaultImage);
		}		
		return super.create(dto);
	}
	
	@GetMapping("/not-link-courses")
	public ResponseEntity<Object> getPostsNotLinkWithCourses() {
		return postService.listAllPostsNotLinkWithCourses();
	}
}
