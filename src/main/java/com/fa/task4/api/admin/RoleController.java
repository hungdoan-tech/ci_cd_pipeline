package com.fa.task4.api.admin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fa.task4.dto.RoleDTO;
import com.fa.task4.entity.Role;

@RestController
@RequestMapping("/api/roles")
public class RoleController extends CRUDController<Role, RoleDTO>{
	
}
