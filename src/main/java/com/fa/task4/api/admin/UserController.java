package com.fa.task4.api.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fa.task4.dto.Credential;
import com.fa.task4.dto.JwtResponse;
import com.fa.task4.dto.UserDTO;
import com.fa.task4.entity.User;
import com.fa.task4.service.IUserService;
import com.fa.task4.utils.JWTUtils;

@RestController
@RequestMapping("/api/users")
public class UserController extends CRUDController<User, UserDTO>{	
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private JWTUtils jwtTokenUtil;

	@Autowired
	private UserDetailsService userDetailsService;

	@PostMapping("/authenticate")
	public ResponseEntity<Object> createAuthenticationToken(@RequestBody Credential authenticationRequest) throws Exception {
		this.userService.authenticate(authenticationRequest.getUsernameOrEmail(), authenticationRequest.getPassword());		

		UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsernameOrEmail());

		final String token = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new JwtResponse(token));
	}
}
