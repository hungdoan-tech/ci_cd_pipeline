package com.fa.task4.api.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fa.task4.api.admin.CategoryController;
import com.fa.task4.service.ICategoryService;

@RestController
@RequestMapping("/api/web/categories")
public class WebCategoryController {
	
	@Autowired
	private ICategoryService categoryService;
	
	@Autowired
	private CategoryController categoryController;
	
	@GetMapping
	public ResponseEntity<Object> getAll() {
		return categoryController.getAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> get(@PathVariable(name = "id") Integer id) {
		return categoryController.get(id);
	}
	
	@GetMapping("/{categoryId}/sub-categories")
	public ResponseEntity<Object> getSubCategory(@PathVariable(name = "categoryId") Integer id) {
		return categoryService.getSubCategories(id);
	}
	
	@GetMapping("/{categoryId}/posts/paging")
	public ResponseEntity<Object> getPostPagingInACategory(
			@PathVariable(name = "categoryId") Integer id,
			@RequestParam(name = "page", defaultValue = "0") Integer page,
			@RequestParam(name = "size", defaultValue = "5") Integer size, 
			@RequestParam(name = "sort_by", defaultValue = "id") String sortBy, 
			@RequestParam(name = "sort_type", defaultValue = "desc") String sortType) {
		
		return categoryService.getPostsInACategory(id, page, size, sortBy, sortType);
	}
	
	@GetMapping("/{categoryId}/courses/paging")
	public ResponseEntity<Object> getCoursePagingInACategory(
			@PathVariable(name = "categoryId") Integer id,
			@RequestParam(name = "page", defaultValue = "0") Integer page,
			@RequestParam(name = "size", defaultValue = "5") Integer size, 
			@RequestParam(name = "sort_by", defaultValue = "id") String sortBy, 
			@RequestParam(name = "sort_type", defaultValue = "desc") String sortType) {
		
		return this.categoryService.getCoursesInACategory(id, page, size, sortBy, sortType);
	}	
}
