package com.fa.task4.api.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fa.task4.api.admin.CommentController;
import com.fa.task4.dto.CommentDTO;

@RestController
@RequestMapping("/api/web/comments")
public class WebCommentController {
	
	@Autowired
	private CommentController commentController;
	
	@GetMapping
	public ResponseEntity<Object> getAll() {
		return commentController.getAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> get(@PathVariable(name = "id") Integer id) {
		return commentController.get(id);
	}
	
	@PostMapping
	public ResponseEntity<Object> create(@Valid @RequestBody CommentDTO comment) {
		return this.commentController.create(comment);
	}
}
