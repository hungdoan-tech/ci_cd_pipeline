package com.fa.task4.api.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fa.task4.api.admin.CourseController;
import com.fa.task4.entity.Invoice;
import com.fa.task4.mapper.InvoiceMapper;
import com.fa.task4.service.ICourseService;

@RestController
@RequestMapping("/api/web/courses")
public class WebCourseController {
	
	@Autowired
	private CourseController courseController;
	
	@Autowired
	private ICourseService courseService;
	
	@Autowired
	private InvoiceMapper invoiceMapper;
	
	@GetMapping
	public ResponseEntity<Object> getAll() {
		return courseController.getAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> get(@PathVariable(name = "id") Integer id) {
		return courseController.get(id);
	}

	@GetMapping("/{id}/invoices")
	public ResponseEntity<Object> getAllInvoicesWhichBuyingACourse(@PathVariable(name = "id") Integer id){
		List<Invoice> invoices = this.courseService.getAllInvoicesBuyingACourse(id);
		return ResponseEntity.ok().body(invoiceMapper.convertEntitiesToDTOs(invoices));		
	}
		
	@GetMapping("/paging")
	public ResponseEntity<Object> getPage(
			@RequestParam(name = "page", defaultValue = "0") Integer page,
			@RequestParam(name = "size", defaultValue = "10") Integer size,
			@RequestParam(name = "sort_by", defaultValue = "id") String sortBy,
			@RequestParam(name = "sort_type", defaultValue = "asc") String sortType) {
		return courseController.getPage(page, size, sortBy, sortType);
	}
}
