package com.fa.task4.api.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fa.task4.api.admin.InvoiceController;
import com.fa.task4.dto.InvoiceDTO;

@RestController
@RequestMapping("/api/web/invoices")
public class WebInvoiceController {
	
	@Autowired
	private InvoiceController invoiceController;
	
	@GetMapping
	public ResponseEntity<Object> getAll() {
		return invoiceController.getAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> get(@PathVariable(name = "id") Integer id) {
		return invoiceController.get(id);
	}
		
	@PostMapping
	public ResponseEntity<Object> create(@Valid @RequestBody InvoiceDTO invoice) {
		return this.invoiceController.create(invoice);
	}
}
