package com.fa.task4.api.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fa.task4.api.admin.PostController;
import com.fa.task4.service.IPostService;

@RestController
@RequestMapping("/api/web/posts")
@CrossOrigin(origins = "http://localhost:8090")
public class WebPostController {
	
	@Autowired
	private IPostService postService;
	
	@Autowired
	private PostController postController;
	
	@GetMapping
	public ResponseEntity<Object> getAll(@PathVariable(name = "postId") Integer id){
		return this.postController.getAll();
	}
	
	@GetMapping("/{postId}")
	public ResponseEntity<Object> getOne(@PathVariable(name = "postId") Integer id){		
		return this.postController.get(id);
	}
	
	@GetMapping("/{postId}/comments")
	public ResponseEntity<Object> getAllCommentInAPost(@PathVariable(name = "postId") Integer id){		
		return this.postService.listAllCommentInAPostByID(id);
	}
	
	@GetMapping("/search")
	public ResponseEntity<Object> getPostHaveTitleLikeKey(
			@RequestParam(name = "keyword") String keyword, 
			@RequestParam(name = "page", defaultValue = "0") Integer page) {
		return this.postService.listAllPostLikeSearchKey(keyword, page, 5, "id", "desc");
	}
}
