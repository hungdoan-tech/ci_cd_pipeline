package com.fa.task4.aspect;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
    @Pointcut("within(@org.springframework.stereotype.Repository *) "
    		+ "|| within(@org.springframework.stereotype.Service *) "
    		+ "|| within(@org.springframework.web.bind.annotation.RestController *)")
	public void springBeanPointcut() {        
	}
	
	@Pointcut("within(com.fa.task4.service.impl..*) "
			+ "|| within(com.fa.task4.repository..*) "
			+ "|| within(com.fa.task4.api..*)")
	public void applicationPackagePointcut() {    	
	}
	
//	@Around("execution(* com.fa.task4.service.impl.CourseService.*(..))")
//	@Around("within(com.fa.task4.service.impl..*) || within(com.fa.task4.repository..*) || within(com.fa.task4.api..*)")
	@Around("applicationPackagePointcut() || springBeanPointcut()")
	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
		log.debug("Enter: {}.{}() with argument[s] = {}",
				joinPoint.getSignature().getDeclaringTypeName(),
				joinPoint.getSignature().getName(),
				Arrays.toString(joinPoint.getArgs()));
		try {
			Object result = joinPoint.proceed();
			log.debug("Exit: {}.{}() with result = {}", 
					joinPoint.getSignature().getDeclaringTypeName(),
					joinPoint.getSignature().getName(), 
					result);
			return result;
		} catch (IllegalArgumentException e) {
			log.error("Illegal argument: {} in {}.{}()", 
					Arrays.toString(joinPoint.getArgs()),
					joinPoint.getSignature().getDeclaringTypeName(), 
					joinPoint.getSignature().getName());
			throw e;
		}
	}

	@AfterThrowing(pointcut = "applicationPackagePointcut() || springBeanPointcut()", throwing = "error")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {
		log.error("Exception in {}.{}() with cause = {} \n StackTrace: {}", 
				joinPoint.getSignature().getDeclaringTypeName(),
				joinPoint.getSignature().getName(), 
				error.getClass().toString() + ":" + error.getMessage(),
				error.getStackTrace());
	}
}