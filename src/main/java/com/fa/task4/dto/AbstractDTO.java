package com.fa.task4.dto;

public abstract class AbstractDTO<T> {
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "AbstractDTO [id=" + id + "]";
	}
	
}
