package com.fa.task4.dto;

import com.fa.task4.entity.Category;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CategoryDTO extends AbstractDTO<Category> {

	private String name;
	
	@JsonInclude(value = Include.NON_NULL)
	@JsonProperty("parent_category_id")
	private Integer parentCategoryId;
	
	@JsonInclude(value = Include.NON_NULL)
	@JsonProperty("parent_category")
	private CategoryDTO parentCategory;
	
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public CategoryDTO getParentCategory() {
		return parentCategory;
	}

	public void setParentCategory(CategoryDTO parentCategory) {
		this.parentCategory = parentCategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public CategoryDTO() {
		super();
	}

	@Override
	public String toString() {
		return "CategoryDTO [name=" + name + ", parentCategoryId=" + parentCategoryId + ", parentCategory="
				+ parentCategory + "]";
	}
}
