package com.fa.task4.dto;

import com.fa.task4.entity.Comment;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CommentDTO extends AbstractDTO<Comment> {

	@JsonProperty("user_name")
	private String userName;
	
	private String email;

	private String content;

	@JsonProperty("post_id")
	@JsonInclude(value = Include.NON_NULL)
	private Integer postId;
	
	@JsonInclude(value = Include.NON_NULL)
	@JsonProperty("post")
	private PostDTO postDTO;
	
	@JsonProperty("created_at")
	private String createdAt;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getPostId() {
		return postId;
	}

	public void setPostId(Integer postId) {
		this.postId = postId;
	}
	
	public PostDTO getPostDTO() {
		return postDTO;
	}

	public void setPostDTO(PostDTO postDTO) {
		this.postDTO = postDTO;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public CommentDTO() {
		super();
	}

	@Override
	public String toString() {
		return "CommentDTO [userName=" + userName + ", email=" + email + ", content=" + content + ", postId=" + postId
				+ ", postDTO=" + postDTO + ", createdAt=" + createdAt + "]";
	}
}
