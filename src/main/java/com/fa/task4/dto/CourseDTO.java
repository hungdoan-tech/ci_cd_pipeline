package com.fa.task4.dto;

import com.fa.task4.entity.Course;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CourseDTO extends AbstractDTO<Course> {
	private String name;
	private String type;
	private Integer price;
	private Integer discount;
	private Boolean hide;
	
	@JsonProperty("post_id")
	@JsonInclude(value = Include.NON_NULL)
	private Integer postId;
	
	private PostDTO post;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Integer getDiscount() {
		return discount;
	}
	public void setDiscount(Integer discount) {
		this.discount = discount;
	}
	public Boolean getHide() {
		return hide;
	}
	public void setHide(Boolean hide) {
		this.hide = hide;
	}
	public Integer getPostId() {
		return postId;
	}
	public void setPostId(Integer postId) {
		this.postId = postId;
	}
	public PostDTO getPost() {
		return post;
	}
	public void setPost(PostDTO post) {
		this.post = post;
	}
	@Override
	public String toString() {
		return "CourseDTO [name=" + name + ", type=" + type + ", price=" + price + ", discount=" + discount + ", hide="
				+ hide + ", postId=" + postId + ", post=" + post + "]";
	}	
}
