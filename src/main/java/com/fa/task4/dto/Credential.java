
package com.fa.task4.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Credential implements Serializable {
	
	private static final long serialVersionUID = -3638636095188849643L;

	@JsonProperty("username")
	private String usernameOrEmail;
	
	private String password;

	public String getUsernameOrEmail() {
		return usernameOrEmail;
	}

	public void setUsernameOrEmail(String usernameOrEmail) {
		this.usernameOrEmail = usernameOrEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Credential(String usernameOrEmail, String password) {
		super();
		this.usernameOrEmail = usernameOrEmail;
		this.password = password;
	}

	public Credential() {
		super();
	}

	@Override
	public String toString() {
		return "Credential [usernameOrEmail=" + usernameOrEmail + ", password=" + password + "]";
	}
	
}
