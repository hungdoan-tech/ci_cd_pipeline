package com.fa.task4.dto;

import java.util.Arrays;

public class Ids {
	private Integer[] ids;

	public Integer[] getIds() {
		return ids;
	}

	public void setIds(Integer[] ids) {
		this.ids = ids;
	}

	@Override
	public String toString() {
		return "Ids [ids=" + Arrays.toString(ids) + "]";
	}
}
