package com.fa.task4.dto;

import com.fa.task4.entity.Invoice;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InvoiceDTO extends AbstractDTO<Invoice> {
	@JsonProperty("customer_name")
	private String customerName;
	
	private String email;
	
	@JsonProperty("grand_total")
	private Integer grandTotal;
	
	private Integer discount;
	
	@JsonProperty("course_id")
	@JsonInclude(value = Include.NON_NULL)
	private Integer courseId;
	
	private CourseDTO course;

	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Integer grandTotal) {
		this.grandTotal = grandTotal;
	}
	public Integer getCourseId() {
		return courseId;
	}
	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}
	public CourseDTO getCourse() {
		return course;
	}
	public void setCourse(CourseDTO course) {
		this.course = course;
	}
	public Integer getDiscount() {
		return discount;
	}
	public void setDiscount(Integer discount) {
		this.discount = discount;
	}
	@Override
	public String toString() {
		return "InvoiceDTO [customerName=" + customerName + ", email=" + email + ", grandTotal=" + grandTotal
				+ ", discount=" + discount + ", courseId=" + courseId + ", course=" + course + "]";
	}	
}
