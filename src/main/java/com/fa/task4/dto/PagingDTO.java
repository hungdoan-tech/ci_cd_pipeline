package com.fa.task4.dto;

import java.util.List;


public class PagingDTO<T> {
	
	List<AbstractDTO<T>> list;	
	private int size;
    private long totalElements;    
    private int totalPages;
    private int numberOfElement;
    private int currentPage;    
    private boolean isFirst;
    private boolean isLast;        
    private  boolean isEmpty;
	public List<AbstractDTO<T>> getList() {
		return list;
	}
	public void setList(List<AbstractDTO<T>> list) {
		this.list = list;
	}
	public long getTotalElements() {
		return totalElements;
	}
	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	public int getNumberOfElement() {
		return numberOfElement;
	}
	public void setNumberOfElement(int numberOfElement) {
		this.numberOfElement = numberOfElement;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public boolean isFirst() {
		return isFirst;
	}
	public void setFirst(boolean isFirst) {
		this.isFirst = isFirst;
	}
	public boolean isLast() {
		return isLast;
	}
	public void setLast(boolean isLast) {
		this.isLast = isLast;
	}
	public boolean isEmpty() {
		return isEmpty;
	}
	public void setEmpty(boolean isEmpty) {
		this.isEmpty = isEmpty;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	@Override
	public String toString() {
		return "PagingDTO [list=" + list + ", size=" + size + ", totalElements=" + totalElements + ", totalPages="
				+ totalPages + ", numberOfElement=" + numberOfElement + ", currentPage=" + currentPage + ", isFirst="
				+ isFirst + ", isLast=" + isLast + ", isEmpty=" + isEmpty + "]";
	}   
}
