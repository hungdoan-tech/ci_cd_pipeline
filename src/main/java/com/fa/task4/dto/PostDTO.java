package com.fa.task4.dto;

import com.fa.task4.entity.Post;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PostDTO extends AbstractDTO<Post> {
	
	@JsonProperty("content")
	private String content;

	@JsonProperty("title")
	private String title;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("cover_image")
	private String coverImage;
	
	@JsonProperty("description")
	private String description;

	@JsonInclude(value = Include.NON_NULL)
	@JsonProperty("author_id")
	private Integer authorId;

	@JsonInclude(value = Include.NON_NULL)
	@JsonProperty("category_id")
	private Integer categoryId;

	@JsonInclude(value = Include.NON_NULL)
	@JsonProperty("author")
	private UserDTO author;

	@JsonInclude(value = Include.NON_NULL)
	@JsonProperty("category")
	private CategoryDTO category;

	public Integer getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Integer authorId) {
		this.authorId = authorId;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCoverImage() {
		return coverImage;
	}

	public void setCoverImage(String coverImage) {
		this.coverImage = coverImage;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public UserDTO getAuthor() {
		return author;
	}

	public void setAuthor(UserDTO author) {
		this.author = author;
	}

	public CategoryDTO getCategory() {
		return category;
	}

	public void setCategory(CategoryDTO category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "PostDTO [content=" + content + ", title=" + title + ", createdAt=" + createdAt + ", coverImage="
				+ coverImage + ", description=" + description + ", authorId=" + authorId + ", categoryId=" + categoryId
				+ ", author=" + author + ", category=" + category + "]";
	}

}
