package com.fa.task4.dto;

import com.fa.task4.entity.Role;

public class RoleDTO extends AbstractDTO<Role> {

	private String name;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "RoleDTO [name=" + name + "]";
	}
	
}
