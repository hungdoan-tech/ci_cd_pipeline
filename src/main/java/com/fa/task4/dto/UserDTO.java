package com.fa.task4.dto;

import com.fa.task4.entity.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDTO extends AbstractDTO<User> {
	private String username;
		
	private String email;
	
	@JsonInclude(value = Include.NON_NULL)
	private String password;
	
	@JsonInclude(value = Include.NON_NULL)
	private String matchingPassword;
	
	private String name;
	
	@JsonProperty("role_id")
	@JsonInclude(value = Include.NON_NULL)	
	private Integer roleId;
	
	private RoleDTO role;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public RoleDTO getRole() {
		return role;
	}

	public void setRole(RoleDTO role) {
		this.role = role;
	}

	public String getMatchingPassword() {
		return matchingPassword;
	}

	public void setMatchingPassword(String matchingPassword) {
		this.matchingPassword = matchingPassword;
	}

	@Override
	public String toString() {
		return "UserDTO [username=" + username + ", email=" + email + ", password=" + password + ", matchingPassword="
				+ matchingPassword + ", name=" + name + ", roleId=" + roleId + ", role=" + role + "]";
	}		
}
