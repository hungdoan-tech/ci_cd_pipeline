package com.fa.task4.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "invoices")
public class Invoice {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "customer_name", nullable = false)
	private String customerName;
	
	@Column(name = "email", nullable = false)
	private String email;
	
	@Column(name = "discount", nullable = true)
	private Integer discount;
	
	@Column(name = "grand_total", nullable = false)
	private Integer grandTotal;
	
	@OneToOne
	@JoinColumn(name = "course_id")
	private Course course;
	
	@Column(nullable = false)
	private Boolean active = true;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public Integer getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Integer grandTotal) {
		this.grandTotal = grandTotal;
	}

	@Override
	public String toString() {
		return "Invoice [id=" + id + ", customerName=" + customerName + ", email=" + email + ", discount=" + discount
				+ ", grandTotal=" + grandTotal + ", course=" + course + ", active=" + active + "]";
	}
}
