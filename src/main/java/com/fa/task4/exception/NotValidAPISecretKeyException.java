package com.fa.task4.exception;

import org.springframework.security.core.AuthenticationException;

public class NotValidAPISecretKeyException extends AuthenticationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotValidAPISecretKeyException() {
		super("This api secret key is not valid ");
	}
	
	public NotValidAPISecretKeyException(String message) {
		super(message);
	}
}
