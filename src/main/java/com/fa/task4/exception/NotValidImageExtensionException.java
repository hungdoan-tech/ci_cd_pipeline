package com.fa.task4.exception;

public class NotValidImageExtensionException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4084276439415962413L;

	public NotValidImageExtensionException() {
		super("This received image has a invalid extent	`ion type ");
	}
	
	public NotValidImageExtensionException(String message) {
		super(message);
	}
}
