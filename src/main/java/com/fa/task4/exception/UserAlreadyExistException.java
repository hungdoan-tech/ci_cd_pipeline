package com.fa.task4.exception;

public class UserAlreadyExistException extends RuntimeException {
	
	private static final long serialVersionUID = 4450249768641753702L;

	public UserAlreadyExistException() {
		super("This username or password of this user has already exist");
	}
	
	public UserAlreadyExistException(String message) {
		super(message);
	}
}
