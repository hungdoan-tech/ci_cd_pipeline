package com.fa.task4.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fa.task4.dto.AbstractDTO;
import com.fa.task4.dto.CategoryDTO;
import com.fa.task4.entity.Category;
import com.fa.task4.repository.CategoryRepository;

@Component
public class CategoryMapper implements IMapper<Category>{

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	public Category convertDTOToEntity(AbstractDTO<Category> dto) {
		Category category = null;
		if(dto instanceof CategoryDTO) {
			CategoryDTO categoryDTO = (CategoryDTO)(dto);									
			category = new Category();
			
			if(categoryDTO.getId() != null) {
				category = this.categoryRepository.findById(categoryDTO.getId()).orElse(null);
				if(category == null) return null;
			}
			if(categoryDTO.getName() != null) category.setName(categoryDTO.getName());
			
			Integer jsonCategoryId = categoryDTO.getParentCategoryId();
			if(jsonCategoryId != null) {
				Category parentCategory = this.categoryRepository.findById(jsonCategoryId).orElse(null);
				if(parentCategory != null) {
					category.setParentCategory(parentCategory);
				}				
			}			
		}
		return category;
	}

	@Override
	public AbstractDTO<Category> convertEntityToDTO(Category entity) {		
		CategoryDTO categoryDTO = new CategoryDTO();
		categoryDTO.setId(entity.getId());
		categoryDTO.setName(entity.getName());
		
		if(entity.getParentCategory() != null) {
			CategoryDTO presentationCategoryDTO = (CategoryDTO) this.convertEntityToDTO(entity.getParentCategory());
			categoryDTO.setParentCategory(presentationCategoryDTO);
		}			
		return categoryDTO;
	}

	@Override
	public List<AbstractDTO<Category>> convertEntitiesToDTOs(List<Category> entities) {
		List<AbstractDTO<Category>> listCategoryDTOs = new ArrayList<AbstractDTO<Category>>();
		for(Category item : entities) {
			listCategoryDTOs.add((CategoryDTO) this.convertEntityToDTO(item));
		}
		return listCategoryDTOs;
	}
}
