package com.fa.task4.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fa.task4.dto.AbstractDTO;
import com.fa.task4.dto.CommentDTO;
import com.fa.task4.dto.PostDTO;
import com.fa.task4.entity.Comment;
import com.fa.task4.entity.Post;
import com.fa.task4.repository.CommentRepository;
import com.fa.task4.repository.PostRepository;
import com.fa.task4.utils.Formatter;

@Component
public class CommentMapper implements IMapper<Comment>{
	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private PostMapper postMapper;
	
	@Autowired
	private CommentRepository commentRepository;
	
	@Override
	public Comment convertDTOToEntity(AbstractDTO<Comment> dto) {
		if (dto instanceof CommentDTO) {
			Comment comment = new Comment();
			CommentDTO commentDTO = (CommentDTO) dto;
			
			if (commentDTO.getId() != null) {
				comment = commentRepository.getOne(commentDTO.getId());
				if (comment == null) return null;
			}
			
			if (commentDTO.getContent() != null) comment.setContent(commentDTO.getContent());
			if (commentDTO.getUserName() != null) comment.setUserName(commentDTO.getUserName());
			if (commentDTO.getEmail() != null) comment.setEmail(commentDTO.getEmail());
			
			if (commentDTO.getPostId() != null) {
				Post post = postRepository.getOne(commentDTO.getPostId());
				if (post != null) comment.setPost(post);
			}
			
			comment.setCreatedAt(new Date());
			
			return comment;
		}
		return null;
	}

	@Override
	public AbstractDTO<Comment> convertEntityToDTO(Comment comment) {
		CommentDTO commentDTO = new CommentDTO();
		commentDTO.setId(comment.getId());
		commentDTO.setUserName(comment.getUserName());
		commentDTO.setEmail(comment.getEmail());
		commentDTO.setContent(comment.getContent());
		commentDTO.setCreatedAt(Formatter.toDateTime(comment.getCreatedAt()));
		
		if(comment.getPost() != null) commentDTO.setPostDTO((PostDTO)this.postMapper.convertEntityToDTO(comment.getPost()));
		return commentDTO;
	}

	@Override
	public List<AbstractDTO<Comment>> convertEntitiesToDTOs(List<Comment> comments) {
		List<AbstractDTO<Comment>> commentDTOs = new ArrayList<>();
		for (Comment comment : comments) {
			commentDTOs.add(convertEntityToDTO(comment));
		}
		return commentDTOs;
	}
}
