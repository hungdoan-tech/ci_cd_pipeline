package com.fa.task4.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fa.task4.dto.AbstractDTO;
import com.fa.task4.dto.CourseDTO;
import com.fa.task4.dto.PostDTO;
import com.fa.task4.entity.Course;
import com.fa.task4.entity.Post;
import com.fa.task4.repository.CourseRepository;
import com.fa.task4.repository.PostRepository;

@Component
public class CourseMapper implements IMapper<Course>{
	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private CourseRepository courseRepository;
	
	@Autowired
	private IMapper<Post> postMapper;	
	
	@Override
	public Course convertDTOToEntity(AbstractDTO<Course> dto) {
		if (dto instanceof CourseDTO) {
			Course course = new Course();
			CourseDTO courseDTO = (CourseDTO) dto;
			
			if (courseDTO.getId() != null) {
				course = courseRepository.getOne(courseDTO.getId());
				if (course == null) return null;
			}
			
			if (courseDTO.getName() != null) course.setName(courseDTO.getName());
			if (courseDTO.getPrice() != null) course.setPrice(courseDTO.getPrice());
			if (courseDTO.getDiscount() != null) course.setDiscount(courseDTO.getDiscount());
			if (courseDTO.getHide() != null) course.setActive(courseDTO.getHide());

			if (courseDTO.getPostId() != null) {
				Post post = postRepository.getOne(courseDTO.getPostId());
				if (post != null) course.setPost(post);
			}
			return course;
		}
		return null;
	}

	@Override
	public AbstractDTO<Course> convertEntityToDTO(Course course) {
		CourseDTO courseDTO = new CourseDTO();
		courseDTO.setId(course.getId());
		courseDTO.setName(course.getName());
		courseDTO.setPrice(course.getPrice());
		courseDTO.setDiscount(course.getDiscount());
		courseDTO.setHide(course.getActive());
		courseDTO.setPost((PostDTO) postMapper.convertEntityToDTO(course.getPost()));
		return courseDTO;
	}

	@Override
	public List<AbstractDTO<Course>> convertEntitiesToDTOs(List<Course> courses) {
		List<AbstractDTO<Course>> courseDTOs = new ArrayList<>();
		for (Course course : courses) {
			courseDTOs.add(convertEntityToDTO(course));
		}
		return courseDTOs;
	}
}
