package com.fa.task4.mapper;

import java.util.List;

import com.fa.task4.dto.AbstractDTO;

public interface IMapper<T> {
	T convertDTOToEntity(AbstractDTO<T> dto);
	AbstractDTO<T> convertEntityToDTO(T entity);
	List<AbstractDTO<T>> convertEntitiesToDTOs(List<T> entities);
}
