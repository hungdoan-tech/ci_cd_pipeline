package com.fa.task4.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fa.task4.dto.AbstractDTO;
import com.fa.task4.dto.CourseDTO;
import com.fa.task4.dto.InvoiceDTO;
import com.fa.task4.entity.Course;
import com.fa.task4.entity.Invoice;
import com.fa.task4.repository.CourseRepository;
import com.fa.task4.repository.InvoiceRepository;

@Component
public class InvoiceMapper implements IMapper<Invoice>{
	
	@Autowired
	private CourseRepository courseRepository;
	
	@Autowired
	private InvoiceRepository invoiceRepository;
	
	@Autowired
	private CourseMapper courseMapper;
	
	@Override
	public Invoice convertDTOToEntity(AbstractDTO<Invoice> dto) {
		if (dto instanceof InvoiceDTO) {
			Invoice invoice = new Invoice();
			InvoiceDTO invoiceDTO = (InvoiceDTO) dto;
			
			if (invoiceDTO.getId() != null) {
				invoice = invoiceRepository.getOne(invoiceDTO.getId());
				if (invoice == null) return null;
			}
			
			if (invoiceDTO.getCustomerName() != null) invoice.setCustomerName(invoiceDTO.getCustomerName());
			if (invoiceDTO.getEmail() != null) invoice.setEmail(invoiceDTO.getEmail());
			if (invoiceDTO.getGrandTotal() != null) invoice.setGrandTotal(invoiceDTO.getGrandTotal());
			if (invoiceDTO.getDiscount() != null) invoice.setDiscount(invoiceDTO.getDiscount());

			if (invoiceDTO.getCourseId() != null) {
				Course course = courseRepository.getOne(invoiceDTO.getCourseId());
				if (course != null) invoice.setCourse(course);
			}
			return invoice;
		}
		return null;
	}

	@Override
	public AbstractDTO<Invoice> convertEntityToDTO(Invoice invoice) {
		InvoiceDTO invoiceDTO = new InvoiceDTO();
		invoiceDTO.setId(invoice.getId());
		invoiceDTO.setCustomerName(invoice.getCustomerName());
		invoiceDTO.setEmail(invoice.getEmail());
		invoiceDTO.setGrandTotal(invoice.getGrandTotal());
		invoiceDTO.setCourse((CourseDTO) courseMapper.convertEntityToDTO(invoice.getCourse()));
		return invoiceDTO;
	}

	@Override
	public List<AbstractDTO<Invoice>> convertEntitiesToDTOs(List<Invoice> invoices) {
		List<AbstractDTO<Invoice>> invoiceDTOs = new ArrayList<>();
		for (Invoice invoice : invoices) {
			invoiceDTOs.add(convertEntityToDTO(invoice));
		}
		return invoiceDTOs;
	}
}
