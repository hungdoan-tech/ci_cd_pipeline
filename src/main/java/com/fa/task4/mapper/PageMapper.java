package com.fa.task4.mapper;

import org.springframework.data.domain.Page;

import com.fa.task4.dto.PagingDTO;

public class PageMapper<T> {
	private IMapper<T> mapper;
	
	public PageMapper(IMapper<T> mapper) {
		super();
		this.mapper = mapper;
	}

	public PagingDTO<T> map(Page<T> inPage){
		
		PagingDTO<T> outPage = new PagingDTO<>();
		outPage.setList(mapper.convertEntitiesToDTOs(inPage.getContent()));
		outPage.setSize(inPage.getSize());
		outPage.setTotalPages(inPage.getTotalPages());
		outPage.setCurrentPage(inPage.getNumber());
		outPage.setTotalElements(inPage.getTotalElements());
		outPage.setNumberOfElement(inPage.getNumberOfElements());
		outPage.setFirst(outPage.getCurrentPage() == 0);
		outPage.setLast(outPage.getCurrentPage() >= inPage.getTotalPages()-1);
		outPage.setEmpty(inPage.getTotalElements() == 0);
		return outPage;		
	}
}
