package com.fa.task4.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fa.task4.dto.AbstractDTO;
import com.fa.task4.dto.CategoryDTO;
import com.fa.task4.dto.PostDTO;
import com.fa.task4.dto.UserDTO;
import com.fa.task4.entity.Category;
import com.fa.task4.entity.Post;
import com.fa.task4.entity.User;
import com.fa.task4.repository.CategoryRepository;
import com.fa.task4.repository.PostRepository;
import com.fa.task4.repository.UserRepository;
import com.fa.task4.utils.Formatter;

@Component
public class PostMapper implements IMapper<Post> {

	@Autowired
    private UserRepository userRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private CategoryMapper categoryMapper;
	
	@Autowired
	private UserMapper userMapper;

    @Override
    public Post convertDTOToEntity(AbstractDTO<Post> dto) {
       	Post post = new Post();
    	if (dto instanceof PostDTO) {
            PostDTO postDTO = (PostDTO) dto;
            
            if(postDTO.getId() != null) {
            	post = this.postRepository.findById(postDTO.getId()).orElse(null);            	
            	if (post == null) return null;
            }
            
            if(postDTO.getTitle() != null) post.setTitle(postDTO.getTitle());
            
            if(postDTO.getDescription() != null) post.setDescription(postDTO.getDescription());
            	
            if(postDTO.getContent() != null) post.setContent(postDTO.getContent());
            	
            if(postDTO.getCoverImage() != null) post.setCoverImage(postDTO.getCoverImage());
            	
            post.setCreatedAt(new Date());
            
            Integer jsonAuthorId = postDTO.getAuthorId();
			if(jsonAuthorId != null) {
				User author = this.userRepository.findById(jsonAuthorId).orElse(null);
				if(author != null) {
					post.setUser(author);
				}				
			}
			
			Integer jsonCategoryId = postDTO.getCategoryId();
			if(jsonCategoryId != null) {
				Category category = this.categoryRepository.findById(jsonCategoryId).orElse(null);
				if(category != null) {
					post.setCategory(category);
				}				
			}
        }		
        return post;
    }

 

    @Override
    public AbstractDTO<Post> convertEntityToDTO(Post entity) {
    	PostDTO postDTO = new PostDTO();
    	postDTO.setId(entity.getId());
    	postDTO.setTitle(entity.getTitle());
    	postDTO.setContent(entity.getContent());
    	postDTO.setCoverImage(entity.getCoverImage());   
    	postDTO.setDescription(entity.getDescription());
    	postDTO.setCreatedAt(Formatter.toDateTime(entity.getCreatedAt()));
		if(entity.getCategory() != null) {
			CategoryDTO presentationCategoryDTO = (CategoryDTO) this.categoryMapper.convertEntityToDTO(entity.getCategory());
			postDTO.setCategory(presentationCategoryDTO);
		}		
				
		postDTO.setAuthor((UserDTO) userMapper.convertEntityToDTO(entity.getUser()));			
				
		return postDTO;
    }

    @Override
    public List<AbstractDTO<Post>> convertEntitiesToDTOs(List<Post> posts) {
        List<AbstractDTO<Post>> postDTOs = new ArrayList<>();
        for (Post post : posts) {
            postDTOs.add(convertEntityToDTO(post));
        }
        return postDTOs;
    }
}
