package com.fa.task4.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fa.task4.dto.AbstractDTO;
import com.fa.task4.dto.RoleDTO;
import com.fa.task4.entity.Role;
import com.fa.task4.repository.RoleRepository;

@Component
public class RoleMapper implements IMapper<Role>{
	@Autowired
	private RoleRepository roleRepository;
	
	@Override
	public Role convertDTOToEntity(AbstractDTO<Role> dto) {
		if (dto instanceof RoleDTO) {

			Role role = new Role();
			RoleDTO roleDTO = (RoleDTO) dto;
			System.out.println(roleDTO.getName());
			if (roleDTO.getId() != null) {
				role = roleRepository.getOne(roleDTO.getId());
				if (role == null) return null;
			}
			
			if (roleDTO.getName() != null) role.setName(roleDTO.getName());
		 
			return role;
		}
		return null;
	}

	@Override
	public AbstractDTO<Role> convertEntityToDTO(Role role) {
		RoleDTO roleDTO = new RoleDTO();
		roleDTO.setId(role.getId());
		roleDTO.setName(role.getName());
		return roleDTO;
	}

	@Override
	public List<AbstractDTO<Role>> convertEntitiesToDTOs(List<Role> roles) {
		List<AbstractDTO<Role>> roleDTOs = new ArrayList<>();
		for (Role role : roles) {
			roleDTOs.add(convertEntityToDTO(role));
		}
		return roleDTOs;
	}
	
	

}
