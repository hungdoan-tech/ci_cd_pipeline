package com.fa.task4.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fa.task4.dto.AbstractDTO;
import com.fa.task4.dto.RoleDTO;
import com.fa.task4.dto.UserDTO;
import com.fa.task4.entity.Role;
import com.fa.task4.entity.User;
import com.fa.task4.repository.RoleRepository;
import com.fa.task4.repository.UserRepository;

@Component
public class UserMapper implements IMapper<User>{
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleMapper roleMapper;
	
	@Override
	public User convertDTOToEntity(AbstractDTO<User> dto) {
		if (dto instanceof UserDTO) {
			User user = new User();
			UserDTO userDTO = (UserDTO) dto;
			
			if (userDTO.getId() != null) {
				user = userRepository.getOne(userDTO.getId());
				if (user == null) return null;
			}
			
			if (userDTO.getName() != null) user.setName(userDTO.getName());
			if (userDTO.getEmail() != null) user.setEmail(userDTO.getEmail());
			if (userDTO.getUsername() != null) user.setUsername(userDTO.getUsername());
			if (userDTO.getPassword() != null) user.setPassword(userDTO.getPassword());


			if (userDTO.getRoleId() != null) {
				Role role = roleRepository.getOne(userDTO.getRoleId());
				if (role != null) user.setRole(role);
			}
			
			return user;
		}
		return null;
	}

	@Override
	public AbstractDTO<User> convertEntityToDTO(User user) {
		UserDTO userDTO = new UserDTO();
		userDTO.setId(user.getId());
		userDTO.setName(user.getName());
		userDTO.setEmail(user.getEmail());
		userDTO.setUsername(user.getUsername());
		userDTO.setRole((RoleDTO) roleMapper.convertEntityToDTO(user.getRole()));
		return userDTO;
	}

	@Override
	public List<AbstractDTO<User>> convertEntitiesToDTOs(List<User> users) {
		List<AbstractDTO<User>> userDTOs = new ArrayList<>();
		for (User user : users) {
			userDTOs.add(convertEntityToDTO(user));
		}
		return userDTOs;
	}
	
	

}
