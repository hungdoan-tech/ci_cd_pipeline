package com.fa.task4.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BasicRepository<T> extends JpaRepository<T, Integer>  {
	public List<T> findAllByActiveIsTrue();
	
	public Page<T> findAllByActiveIsTrue(Pageable pageable);
	
	public Optional<T> findByIdAndActiveIsTrue(Integer id);
	
	public void deactiveById(Integer id); 
}
