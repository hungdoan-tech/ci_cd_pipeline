package com.fa.task4.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fa.task4.entity.Category;

public interface CategoryRepository extends BasicRepository<Category> {
	@Override
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update Category c set c.active = 0 where c.id = :id")
	public void deactiveById(@Param("id") Integer id); 
	
	public List<Category> findAllByParentCategoryIdEquals(Integer id);
	
	public List<Category> findAllCategoriesByParentCategoryIsNull();
}
