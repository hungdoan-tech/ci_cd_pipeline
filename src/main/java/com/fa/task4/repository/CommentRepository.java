package com.fa.task4.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fa.task4.entity.Comment;

public interface CommentRepository extends BasicRepository<Comment> {
	@Override
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update Comment c set c.active = 0 where c.id = :id")
	public void deactiveById(@Param("id") Integer id); 
	
	public List<Comment> findAllByPostIdEquals(Integer id);
}
