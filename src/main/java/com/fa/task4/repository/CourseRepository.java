package com.fa.task4.repository;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fa.task4.entity.Course;

public interface CourseRepository extends BasicRepository<Course> {
	@Override
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update Course c set c.active = 0 where c.id = :id")
	public void deactiveById(@Param("id") Integer id);
	
	public Page<Course> findByPostCategoryId(Integer id, Pageable pageable);
}
