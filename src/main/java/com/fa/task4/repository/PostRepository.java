package com.fa.task4.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fa.task4.entity.Post;

public interface PostRepository extends BasicRepository<Post> {
	@Override
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update Post c set c.active = 0 where c.id = :id")
	public void deactiveById(@Param("id") Integer id);
	
	@Query("SELECT p FROM Course c RIGHT JOIN c.post p WHERE c.id IS NULL")
	public List<Post> findByNotLinkWithCourses();
	
	public List<Post> findAllByCategoryIdEquals(Integer categoryId);
	
	public Optional<Post> findByIdAndActiveIsTrue(Integer id);
	
	public Page<Post> findAllByActiveIsTrue(Pageable pageable);
	
	public Page<Post> findByActiveIsTrueAndCategoryId(Integer categoryId, Pageable pageable);
	
	public Page<Post> findByTitleContainingIgnoreCaseAndActiveIsTrue(String title, Pageable pageable);
}
