package com.fa.task4.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fa.task4.entity.Role;

public interface RoleRepository extends BasicRepository<Role> {
	@Override
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update Role c set c.active = 0 where c.id = :id")
	public void deactiveById(@Param("id") Integer id); 
}
