package com.fa.task4.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fa.task4.entity.User;

public interface UserRepository extends BasicRepository<User>{
	@Override
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update User c set c.active = 0 where c.id = :id")
	public void deactiveById(@Param("id") Integer id); 
	
	public Optional<User> findByUsername(String username);
	public Optional<User> findByEmail(String email);
	public Optional<User> findByUsernameOrPassword(String firstKeyword, String secondKeyword);
}
