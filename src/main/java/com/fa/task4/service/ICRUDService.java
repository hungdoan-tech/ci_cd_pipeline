package com.fa.task4.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

public interface ICRUDService<T> {
	public void setupCRUDService();

	public T create(T entity);

	public T getOne(Integer id);

	public List<T> getAll();

	public T update(T entity);

	public Boolean deleteById(Integer id);

	public Boolean deleteByIds(Integer[] ids);

	public ResponseEntity<Object> paging(Integer page, Integer size, String sortBy, String sortType);
}
