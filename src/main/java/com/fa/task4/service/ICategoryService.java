package com.fa.task4.service;

import org.springframework.http.ResponseEntity;

import com.fa.task4.entity.Category;

public interface ICategoryService extends ICRUDService<Category>{
	public ResponseEntity<Object> getAllSubCategory();
	public ResponseEntity<Object> getSubCategories(Integer id);
	public ResponseEntity<Object> getPostsInACategory(Integer id, Integer page, Integer size, String sortBy, String sortType);
	public ResponseEntity<Object> getCoursesInACategory(Integer id, Integer page, Integer size, String sortBy, String sortType);
}
