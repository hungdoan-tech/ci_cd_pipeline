package com.fa.task4.service;

import java.util.List;

import com.fa.task4.entity.Course;
import com.fa.task4.entity.Invoice;

public interface ICourseService extends ICRUDService<Course>{
	List<Invoice> getAllInvoicesBuyingACourse(Integer id);
}
