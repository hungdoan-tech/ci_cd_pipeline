package com.fa.task4.service;

import org.springframework.http.ResponseEntity;

import com.fa.task4.entity.Post;

public interface IPostService extends ICRUDService<Post> {
	public ResponseEntity<Object> listAllCommentInAPostByID(Integer id);	
	public ResponseEntity<Object> listAllPostLikeSearchKey(String key, Integer page, Integer size, String sortBy, String sortType);
	public ResponseEntity<Object> listAllPostsNotLinkWithCourses();	
}
