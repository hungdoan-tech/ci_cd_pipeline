package com.fa.task4.service;

import com.fa.task4.entity.User;

public interface IUserService extends ICRUDService<User>{
	boolean authenticate(String username, String password) throws Exception;
	
	User findOneByUsername(String username);
}
