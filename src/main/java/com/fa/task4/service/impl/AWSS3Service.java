package com.fa.task4.service.impl;

import java.io.File;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.fa.task4.exception.NotValidImageExtensionException;
import com.fa.task4.utils.FileUtils;

@Service
public class AWSS3Service implements IAWSS3Service {
	
	@Autowired
    private AmazonS3 amazonS3;
	
    @Value("${aws.s3.bucket}")
    private String bucketName;
     
    @Override
	@Async
    public String uploadFile(MultipartFile multipartFile) {        
        try {
            File file = FileUtils.convertMultiPartFileToFile(multipartFile);
            
            String extension = FileUtils.getExtension(file.getName());
            if(!FileUtils.checkImageExtension(extension)) {
            	throw new NotValidImageExtensionException();
            }
            
            String outputURL = uploadFileToS3Bucket(bucketName, file);            
            file.delete();
            return outputURL;            
        } catch (AmazonServiceException ex) {
            System.out.println("File upload is failed.");
            System.out.println("Error= {} while uploading file." + ex.getMessage());
            return null;
        }
    }
 
    private String uploadFileToS3Bucket(String bucketName, final File file) {
        String uniqueFileName = LocalDateTime.now() + "_" + file.getName();
        System.out.println("Uploading file with name= " + uniqueFileName);
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, uniqueFileName, file);
        PutObjectResult putObject = amazonS3.putObject(putObjectRequest);        
        return (putObject != null) ? amazonS3.getUrl(bucketName, uniqueFileName).toString() : null;
    }
}
