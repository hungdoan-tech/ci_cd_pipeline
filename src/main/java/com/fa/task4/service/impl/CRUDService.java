package com.fa.task4.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fa.task4.dto.PagingDTO;
import com.fa.task4.mapper.IMapper;
import com.fa.task4.mapper.PageMapper;
import com.fa.task4.repository.BasicRepository;

public abstract class CRUDService<T> {
	@Autowired
	private BasicRepository<T> repository;
	
	@Autowired
	private IMapper<T> mapper;
	
	private PageMapper<T> pageMapper;
	
	@PostConstruct
	public void setupCRUDService() {
		pageMapper = new PageMapper<T>(mapper);
	}

	public T create(T entity) {	
		return repository.save(entity);
	}

	public T getOne(Integer id) {		
		return repository.findByIdAndActiveIsTrue(id).orElse(null);
	}

	public List<T> getAll() {
		return repository.findAllByActiveIsTrue();
	}

	public T update(T entity) {
		return repository.save(entity);
	}

	public Boolean deleteById(Integer id) {
		try {
			System.out.println("Deactive ok");
			repository.deactiveById(id);
			return true;
		} catch(Exception e){
			return false;
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Boolean deleteByIds(Integer[] ids) {
		try {
			for(Integer id : ids) {
				repository.deactiveById(id);
			}	
			return true;
		} catch(Exception e){
			return false;
		}			
	}
	
	public ResponseEntity<Object> paging(Integer page, Integer size, String sortBy, String sortType){
		try {
			Sort sort = sortType.equals("desc") ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
			Pageable pageable = PageRequest.of(page, size, sort);
			Page<T> entityPage = repository.findAllByActiveIsTrue(pageable);
			PagingDTO<T> DTOPage = pageMapper.map(entityPage);
			return ResponseEntity.ok().body(DTOPage);			
		} catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body("Exception: " + e.getClass());
		}
	}
}
