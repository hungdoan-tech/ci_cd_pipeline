package com.fa.task4.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fa.task4.dto.PagingDTO;
import com.fa.task4.entity.Category;
import com.fa.task4.entity.Course;
import com.fa.task4.entity.Post;
import com.fa.task4.mapper.CourseMapper;
import com.fa.task4.mapper.PageMapper;
import com.fa.task4.mapper.PostMapper;
import com.fa.task4.repository.CategoryRepository;
import com.fa.task4.repository.CourseRepository;
import com.fa.task4.repository.PostRepository;
import com.fa.task4.service.ICategoryService;

@Service
public class CategoryService extends CRUDService<Category> implements ICategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private CourseRepository courseRepository;
	
	@Autowired
	private PostMapper postMapper;
	
	private PageMapper<Post> postPageMapper;
	@PostConstruct
	public void setUpPostPageMapper() {
		postPageMapper = new PageMapper<Post>(postMapper);
	}

	@Autowired
	private CourseMapper courseMapper;
	
	private PageMapper<Course> coursePageMapper;
	@PostConstruct
	public void setUp() {
		coursePageMapper = new PageMapper<Course>(courseMapper);
	}
	
	@Override
	public ResponseEntity<Object> getAllSubCategory() {
		try {
			 List<Category> findAllCategoriesByParentCategoryIsNull = this.categoryRepository.findAllCategoriesByParentCategoryIsNull();
			return ResponseEntity.ok().body(findAllCategoriesByParentCategoryIsNull);
		} catch(Exception e) {
			return ResponseEntity.badRequest().body("Exception : "+ e.getClass());
		}
	}

	@Override
	@Cacheable(cacheNames = "subCategories")
	public ResponseEntity<Object> getSubCategories(Integer id){
		try {
			List<Category> findAllByParentCategoryIdEquals = this.categoryRepository.findAllByParentCategoryIdEquals(id);
			return ResponseEntity.ok().body(findAllByParentCategoryIdEquals);
		} catch(Exception e) {
			return ResponseEntity.badRequest().body("Exception : "+ e.getClass());
		}
	}
	
	@Override
	public ResponseEntity<Object> getPostsInACategory(Integer id, Integer page, Integer size, String sortBy, String sortType) {
		try {
			Sort sort = sortType.equals("desc") ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
			Pageable pageable = PageRequest.of(page, size, sort);
			Page<Post> postPage = this.postRepository.findByActiveIsTrueAndCategoryId(id, pageable);
			PagingDTO<Post> map = this.postPageMapper.map(postPage);
			return ResponseEntity.ok().body(map);			
		} catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body("Exception: " + e.getClass());
		}
	}
	
	@Override
	public ResponseEntity<Object> getCoursesInACategory(Integer id, Integer page, Integer size, String sortBy, String sortType) {
		try {
			Sort sort = sortType.equals("desc") ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
			Pageable pageable = PageRequest.of(page, size, sort);
			Page<Course> postPage = this.courseRepository.findByPostCategoryId(id, pageable);
			PagingDTO<Course> map = this.coursePageMapper.map(postPage);
			return ResponseEntity.ok().body(map);			
		} catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body("Exception: " + e.getClass());
		}
	}

	@Override
	@CacheEvict(cacheNames = "subCategories", allEntries = true)
	public Category update(Category entity) {
		return super.update(entity);
	}

	@Override
	@CacheEvict(cacheNames = "subCategories", allEntries = true)
	public Boolean deleteById(Integer id) {
		return super.deleteById(id);
	}
	
	
}
