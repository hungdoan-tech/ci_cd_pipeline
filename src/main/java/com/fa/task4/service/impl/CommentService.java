package com.fa.task4.service.impl;

import org.springframework.stereotype.Service;

import com.fa.task4.entity.Comment;
import com.fa.task4.service.ICommentService;

@Service
public class CommentService extends CRUDService<Comment> implements ICommentService{

}
