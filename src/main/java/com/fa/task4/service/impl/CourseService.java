package com.fa.task4.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fa.task4.entity.Course;
import com.fa.task4.entity.Invoice;
import com.fa.task4.repository.InvoiceRepository;
import com.fa.task4.service.ICourseService;

@Service
public class CourseService extends CRUDService<Course> implements ICourseService{
	
	@Autowired
	private InvoiceRepository invoiceRepository; 
	
	@Override
	public List<Invoice> getAllInvoicesBuyingACourse(Integer id) {		
		 List<Invoice> invoices = this.invoiceRepository.findByCourseIdIs(id);
		 return invoices;
	}

	@Override
	@CacheEvict(cacheNames = "sidebarData")
	public Course update(Course entity) {
		return super.update(entity);
	}

	@Override
	@CacheEvict(cacheNames = "sidebarData")
	public Boolean deleteById(Integer id) {		
		return super.deleteById(id);
	}

	@Override
	@Cacheable(cacheNames = "sidebarData")
	public ResponseEntity<Object> paging(Integer page, Integer size, String sortBy, String sortType) {		
		return super.paging(page, size, sortBy, sortType);	
	}
}
