package com.fa.task4.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fa.task4.dto.CustomUserDetail;
import com.fa.task4.entity.User;
import com.fa.task4.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String keyword) throws UsernameNotFoundException {
		User user = this.userRepository.findByUsername(keyword).orElseThrow(() -> new UsernameNotFoundException("We dont find any stored account in the db match your typed credential"));
		CustomUserDetail customUserDetail = new CustomUserDetail(user);
		return customUserDetail;
	}
}
