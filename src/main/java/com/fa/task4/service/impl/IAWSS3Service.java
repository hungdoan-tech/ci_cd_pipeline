package com.fa.task4.service.impl;

import org.springframework.web.multipart.MultipartFile;

public interface IAWSS3Service {

	String uploadFile(MultipartFile multipartFile);

}