package com.fa.task4.service.impl;

import org.springframework.stereotype.Service;

import com.fa.task4.entity.Invoice;
import com.fa.task4.service.IInvoiceService;

@Service
public class InvoiceService extends CRUDService<Invoice> implements IInvoiceService{

}
