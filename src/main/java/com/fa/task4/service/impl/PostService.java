package com.fa.task4.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fa.task4.dto.CommentDTO;
import com.fa.task4.dto.PagingDTO;
import com.fa.task4.dto.PostDTO;
import com.fa.task4.entity.Comment;
import com.fa.task4.entity.Post;
import com.fa.task4.mapper.CommentMapper;
import com.fa.task4.mapper.PageMapper;
import com.fa.task4.mapper.PostMapper;
import com.fa.task4.repository.CommentRepository;
import com.fa.task4.repository.PostRepository;
import com.fa.task4.service.IPostService;

@Service
public class PostService extends CRUDService<Post> implements IPostService {
	
	@Autowired
	private CommentRepository commentRepository;
	
	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private CommentMapper commentMapper;
	
	@Autowired
	private PostMapper postMapper;
	
	private PageMapper<Post> postPageMapper;
	
	@PostConstruct
	public void setUp() {
		postPageMapper = new PageMapper<Post>(postMapper);
	}
	
	@Override
	public ResponseEntity<Object> listAllCommentInAPostByID(Integer id){
		try {
			List<Comment> comments = commentRepository.findAllByPostIdEquals(id);
			List<CommentDTO> commentDTOs = new ArrayList<>();
			for (Comment comment: comments) {
				comment.setPost(null);
				commentDTOs.add((CommentDTO) commentMapper.convertEntityToDTO(comment));
			}
			return ResponseEntity.ok().body(commentDTOs);
			
		} catch(Exception e) {
			
			e.printStackTrace();
			return ResponseEntity.badRequest().body("Exception : "+ e.getClass());
			
		}		
	}
	
	public ResponseEntity<Object> listAllPostLikeSearchKey(String key, Integer page, Integer size, String sortBy, String sortType){
		try {
			Sort sort = sortType.equals("desc") ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
			Pageable pageable = PageRequest.of(page, size, sort);
			Page<Post> postPage = this.postRepository.findByTitleContainingIgnoreCaseAndActiveIsTrue(key, pageable);
			PagingDTO<Post> map = postPageMapper.map(postPage);
			return ResponseEntity.ok().body(map);			
		} catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body("Exception: " + e.getClass()); 
		}
	}
	
	@Override
	public ResponseEntity<Object> listAllPostsNotLinkWithCourses(){
		try {
			List<Post> posts = postRepository.findByNotLinkWithCourses();

			List<PostDTO> postDTOs = new ArrayList<>();
			for (Post post: posts) {
				postDTOs.add((PostDTO) postMapper.convertEntityToDTO(post));
			}
			return ResponseEntity.ok().body(postDTOs);
			
		} catch(Exception e) {
			
			e.printStackTrace();
			return ResponseEntity.badRequest().body("Exception : "+ e.getClass());
			
		}		
	}
}
