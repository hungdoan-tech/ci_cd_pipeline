package com.fa.task4.service.impl;

import org.springframework.stereotype.Service;

import com.fa.task4.entity.Role;
import com.fa.task4.service.IRoleService;

@Service
public class RoleService extends CRUDService<Role> implements IRoleService{

}
