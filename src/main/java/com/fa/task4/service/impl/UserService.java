package com.fa.task4.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.fa.task4.entity.User;
import com.fa.task4.exception.UserAlreadyExistException;
import com.fa.task4.repository.UserRepository;
import com.fa.task4.service.IUserService;

@Service
public class UserService extends CRUDService<User> implements IUserService{
	
	@Autowired
	private UserRepository userRepository;	
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Override
	public User create(User entity) {
		User findByUsername = this.userRepository.findByUsername(entity.getUsername()).orElse(null);
		User findByEmail = this.userRepository.findByEmail(entity.getEmail()).orElse(null);
		if(findByEmail != null || findByUsername != null) {
			throw new UserAlreadyExistException();			
		}
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		entity.setPassword(bCryptPasswordEncoder.encode(entity.getPassword()));
		return this.userRepository.save(entity);			
	}
	
	public boolean authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			return true;
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		} 		
	}

	@Override
	public User findOneByUsername(String username) {		
		return this.userRepository.findByUsername(username).orElse(null);	
	}
}
