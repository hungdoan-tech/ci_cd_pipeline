package com.fa.task4.utils;

public enum ERole {
    ROLE_MODERATOR,
    ROLE_ADMIN
}
