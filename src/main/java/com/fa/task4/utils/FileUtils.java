package com.fa.task4.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.multipart.MultipartFile;

public class FileUtils {
	
	public static File convertMultiPartFileToFile(MultipartFile multipartFile) {
		
		File file = new File(multipartFile.getOriginalFilename());
		
		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			outputStream.write(multipartFile.getBytes());
		} catch (final IOException ex) {
			System.out.println("Error converting the multi-part file to file= " + ex.getMessage());			
		}
		return file;
	}

	public static String getExtension(String fileName) {

		int lastDotIndex = fileName.lastIndexOf('.');
		int lastIndex = fileName.length();
		String extension = fileName.substring(lastDotIndex, lastIndex);
		return extension;
	}

	public static boolean checkImageExtension(String extension) {
		
		ArrayList<String> imageExtensions = new ArrayList<String>(Arrays.asList(".png", ".jpeg", ".jpg", ".svg", ".gif"));
		boolean isImage = imageExtensions.contains(extension);
		return isImage;
	}
}
