package com.fa.task4.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Formatter {
	
	public static String toDateTime(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");    	
    	return formatter.format(date);    	
	}
}
